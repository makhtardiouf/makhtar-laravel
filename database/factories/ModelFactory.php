<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Film::class, function (Faker\Generator $faker) {
   
    return [
        'name' => $faker->name,
        'description' =>  $faker->sentence(),
        'release_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'rating' => $faker->numberBetween(1,5),
        'ticket_price' => $faker->numberBetween(10,200),
        'country' =>  $faker->country,
        'genre' =>  $faker->sentence(),
        'photo' =>  'http://lorempixel.com/350/250/?text=User'  ,
    ];
});

$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    return [
           'user_id' => $faker->numberBetween(1, 3),
           'target_id' => $faker->numberBetween(1, 4),
           'content' => $faker->sentence(15),
           'type' => 'films',
        ];
   
   });

   