-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2017 at 01:01 AM
-- Server version: 5.7.20-0ubuntu0.17.04.1
-- PHP Version: 7.0.25-1+ubuntu17.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `makhtar-laravel`
--
CREATE DATABASE IF NOT EXISTS `makhtar-laravel` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `makhtar-laravel`;

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` date NOT NULL,
  `rating` int(11) NOT NULL,
  `ticket_price` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `name`, `description`, `release_date`, `rating`, `ticket_price`, `country`, `genre`, `photo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Conan', 'A powerful Cimmerian warrior, Conan (Jason Momoa) carves a bloody path across the land of Hyboria on a personal vendetta. That soon turns into a an epic battle against evil, for Conan\'s mortal enemy, Khalar Zym (Stephen Lang), seeks the legendary Mask of Acheron. The artifact will enable Khalar Zym to raise his wife from the dead and achieve immortality for himself, but it will also unleash a malevolent force upon the land, and only Conan and his companions can stop it.', '2011-04-11', 5, 56, 'US', 'Action', '/uploads/images/1511620209_conan.jpeg', '2017-11-25 02:07:14', '2017-11-25 05:30:09', NULL),
(2, 'Test', 'test', '4444-04-04', 55, 44, 'FSDF', 'DASFDF', 'DSFA', '2017-11-25 02:48:15', '2017-11-25 02:49:22', '2017-11-25 02:49:22'),
(3, 'Furious 7', 'After defeating international terrorist Owen Shaw, Dominic Toretto (Vin Diesel), Brian O\'Conner (Paul Walker) and the rest of the crew have separated to return to more normal lives. However, Deckard Shaw (Jason Statham), Owen\'s older brother, is thirsty for revenge. A slick government agent offers to help Dom and company take care of Shaw in exchange for their help in rescuing a kidnapped computer hacker who has developed a powerful surveillance program.', '2015-11-03', 5, 44, 'FSDF', 'DASFDF', 'http://via.placeholder.com/350x150', '2017-11-25 02:49:19', '2017-11-25 02:50:30', NULL),
(4, 'Die Hard', 'The Die Hard series is an American action film series that began in 1988 with Die Hard, based on the 1979 novel Nothing Lasts Forever by Roderick Thorp. The series follows the adventures of John McClane (portrayed by Bruce Willis), a New York City and Los Angeles police detective who continually finds himself in the middle of violent crises and intrigues where he is the only hope against disaster.[2]', '1988-01-26', 4, 34, 'US', 'Action', '/uploads/images/1511619769_diehard.jpeg', '2017-11-25 03:09:54', '2017-11-25 05:22:49', NULL),
(5, 'LastNinja', 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.', '1233-04-12', 4, 2, '4', '4444', '/uploads/images/1511623006_ninja.jpg', '2017-11-25 04:19:27', '2017-11-25 06:16:46', NULL),
(6, 'Super Man', 'Two THR film critics rank all the Superman films and spinoffs, including a revamped version of the 1980 sequel (featuring more Marlon Brando) and the deliriously campy \'Supergirl.\'', '2012-02-14', 4, 89, 'US', 'Sci-Fi', '/uploads/images/1511620240_superman.jpg', '2017-11-25 05:25:54', '2017-11-25 05:30:40', NULL),
(7, 'dfds', 'sdfsd', '1222-03-31', 4, 333, 'US', 'Test', '/tmp/phpQDaqQK', '2017-11-25 05:28:01', '2017-11-25 05:29:44', '2017-11-25 05:29:44'),
(8, 'dfds', 'test', '1222-03-31', 4, 333, 'US', 'test', '/uploads/images/1511620175_superman.jpg', '2017-11-25 05:29:35', '2017-11-25 05:30:30', '2017-11-25 05:30:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_25_103126_create_films_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Makhtar', 'makhtar.diouf@gmail.com', '$2y$10$/FLQkohNd5.0OXNET4z2XuHVRCSYRzU2hLARTjjNqsdFFYvaaLT9.', '8W6WLCI10ZwHzH67hkorGgVOlpdc31VovDKBQIwogrl9aATXFQ1VZKq6C1s5', '2017-11-25 01:52:33', '2017-11-25 01:52:33'),
(2, 'Administrator', 'elmakdi@gmail.com', '$2y$10$jZjPmJJb9j7O0mzC.hnizOztqFXIPCrTqoDrQQ.RrlElxEFqeB0Zu', 'yUE64iQnAymM8yf81p5YJlpwy62lN8tCw177E5PmqD1Gd3Qu21SOauEzfwLP', '2017-11-25 02:03:31', '2017-11-25 02:03:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
