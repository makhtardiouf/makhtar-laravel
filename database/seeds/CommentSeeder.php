<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds for Comments.
     *
     * @return void
     */
    public function run()
    {
        try {
            factory('App\Comment', 3)->create();
                     
        } catch (Exception $e) {
            Log::error("Comment seeder error\n".$e->getMessage());
        }
    }
}
