<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public $tables = [ 'films', 'users', 'comments' ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FilmSeeder::class);
        
        $this->call(UsersSeeder::class);
     
        $this->call(CommentSeeder::class);
    }
}
