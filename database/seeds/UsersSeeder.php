<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            factory('App\User', 2)->create();
                     
        } catch (Exception $e) {
            Log::error("Users table seeder error\n".$e->getMessage());
        }
    }
}
