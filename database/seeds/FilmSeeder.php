<?php

use Illuminate\Database\Seeder;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            factory('App\Film', 3)->create();
                     
        } catch (Exception $e) {
            Log::error("Film seeder error: ".$e->getMessage());
        }
    }
}
