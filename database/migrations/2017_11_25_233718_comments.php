<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comments extends Migration
{
    /**
     * Run the migrations for both House houses and Toko requests
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id', false)->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('target_id', false)->unsigned();

            $table->text('type', 10);
        
            $table->text('content');
            $table->timestamps();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            Schema::table('comments', function (Blueprint $table) {
                $this->dropForeignSafely($table, 'comments', 'user_id');
                $this->dropForeignSafely($table, 'comments', 'target_id');
            });

           
        } catch (Exception $e) {
            $msg = "Error migrating comments tables:\n ".$e->getMessage() ."\n";
            echo $msg;
            Log::error($msg);
        }

        Schema::dropIfExists('comments');
       
    }

    public function dropForeignSafely(Blueprint $table, $name, $key)
    {
        if (Schema::hasColumn("$name", $name."_".$key."_foreign")) {
            echo "Got foreign key ".$name."_".$key."\n";
            $table->dropForeign(["$key"]);
        }
    }
}
