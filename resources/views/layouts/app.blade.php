<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }} | @yield('title')</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">    
    
	<?php 
    $loggedIn = Auth()->guard('web')->check();
    echo "<script>
        window.Laravel = ".json_encode(['csrfToken' => csrf_token()]) .         
        ";
        window.Global = ".json_encode([           
            'auth' => $loggedIn ,
            'user_id' => $loggedIn ? Auth::guard('web')->user()->id : 0,
        ]).
        ";
        </script>";
    ?>
</head>

<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/home">{{ config('app.name') }}</a>
			</div>

			<div class="collapse navbar-collapse" id="navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="{{ url('/films') }}">Films</a>
					</li>
					@if (Auth::guest())

					<li>
						<a href="{{ url('/auth/login') }}">Login</a>
					</li>
					<li>
						<a href="{{ url('/auth/register') }}">Register</a>
					</li>
					@else
					<li>
						<a href="{{ url('/films/create') }}">Create Film</a>
					</li>

					<li>
						<a href="#">{{ Auth::user()->name }}</a>
					</li>

					<li>
						<a href="{{ url('/auth/logout') }}">Logout</a>
					</li>
					@endif
				</ul>
			</div>

		</div>
		<!-- /.container-fluid -->
	</nav>

	<div class="container" style="padding:50px;">
		<div class="row" id="app">
			@yield('content')			
		</div>
	</div>
		
	<hr/>

	<div class="container">
		&copy; {{ date('Y') }}. Created by Makhtar Diouf
		<br/>
	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<script src="{{ asset('/js/app.js') }}"></script>
	@yield('scripts')
</body>
</html>