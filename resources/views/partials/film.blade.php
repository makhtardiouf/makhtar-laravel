
<div class="row">
    <div class="col-md-offset-2 col-md-8 well">
        <h3>No. {{ $film->id }}: {{ $film->name }}</h3>

        <p>{{ $film->description }} </p>
        <ul>       
            <li>Rating: {{ $film->rating }} </li>
            <li>Release Date: {{ $film->release_date }} </li>
            <li>Film slug: <a href="/films/{{$film->name}}">films/{{$film->name}}</a></li>
        </ul>

        <img src="{{$film->photo}}" class="film-img">

        @if(Auth::user())
        <div>
            <a href="{{ url('films/' . $film->id . '/edit') }}" class="btn btn-primary btn-sm">Update</a> 

            {{--  <a class="btn btn-primary btn-sm" href="{{ url('films') }}">Films list</a>  --}}

            <a class="btn btn-primary btn-sm" href="{{ url('films/create') }}">Add New Film</a>
        </div>
       
        <div>            
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['films', $film->id],
                    'style' => 'display:inline'
                ]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm pull-right']) !!}
                {!! Form::close() !!}
        </div>
         @endif 

    </div>
</div>


<div class="row">
	<div class="col-md-offset-2 col-md-8 ">
		{{ !empty($films) ? $films->links() : ''}}
	</div>
</div>


<div class="row">
    <div class="col-md-6 col-md-offset-2">
    <hr>
    <h6>Comments:</h6>
        <comments type="films"
            target_id="{!! $film? $film->id : 0!!}"
            label="Comment"></comments>
    </div>
</div>

 @include('partials.comments')
