
<div class="row" style="margin-top: 10px;">
@foreach($comments as $com)   
    <div class="col-md-8 col-md-offset-2">
        <a href="#{{$com->user_id}}">
        <i class="fa fax-2x fa-user-circle" aria-hidden="true"></i>  
        {{$com->user->name}}</a>          
                        
        <span style="margin-left:10px;">{{$com->content}}</span>
        <br>
        <span class="grey-text" style="margin-left:20px;">{{$com->getDateDiff()}}</span>        
    </div>
@endforeach
</div>
