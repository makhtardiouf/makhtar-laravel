@extends('layouts.app') 
@section('content')

<h3>@lang("Page not found")</h3>  

<div class="container">
    <div class="row main">
        <div class="col-md-6 col-offset-md-3">
            <div class="card">
                <div class="alert alert-warning">
                    <p>The requested page was not found</p>                   
                    {{Request::url()}}
                </div>

                <span>{{ $exception->getMessage() }}</span>
            </div>
        </div>
    </div>
</div>

@endsection 
