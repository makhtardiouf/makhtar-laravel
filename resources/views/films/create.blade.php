@extends('layouts.app') 
@section('title') 
Create new Film 
@stop 

@section('content')

<h3>Create New Film</h3>
<div class="jumbotron col-md-8 col-md-offset-2">
	<h6>Register film data</h6>
	<hr/> {!! Form::open(['url' => 'films', 'files' => true, 'class' => 'form-horizontal']) !!}

	<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
		{!! Form::label('name', 'Name: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('name', '
			<p
			class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
		{!! Form::label('description', 'Description: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('description',
			'
			<p class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
		{!! Form::label('release_date', 'Release Date: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::date('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('release_date',
			'
			<p class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('rating') ? 'has-error' : ''}}">
		{!! Form::label('rating', 'Rating: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::number('rating', 'value',['min'=>1 ,'max'=>5, 
				'class' => 'form-control', 'required' => 'required']) !!} 

			{!! $errors->first('rating', '<p class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('ticket_price') ? 'has-error' : ''}}">
		{!! Form::label('ticket_price', 'Ticket Price: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::number('ticket_price', null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('ticket_price',
			'<p class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
		{!! Form::label('country', 'Country: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::text('country', null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('country',
			'
			<p class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group {{ $errors->has('genre') ? 'has-error' : ''}}">
		{!! Form::label('genre', 'Genre: ', ['class' => 'control-label']) !!}
		<div>
			{!! Form::text('genre', null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('genre', '
			<p
			class="help-block">:message</p>') !!}
		</div>
	</div>

	<div class="form-group">
		<label class="control-label">Photo:</label >			
		
		<div>
			<input type="file" name="photo" class="form-control" required="true" >      
			{!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
		</div>

	</div>


	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-3">
			{!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
		</div>
	</div>

{{--  jumbotron  --}}
</div>

{!! Form::close() !!} @if ($errors->any())
<ul class="alert alert-danger">
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
</ul>
@endif @endsection