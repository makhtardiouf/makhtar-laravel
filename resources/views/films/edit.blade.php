@extends('layouts.app')
@section('title')
Edit Film
@stop

@section('content')

    <h3>Edit Film</h3>
    <hr/>

    {!! Form::model($film, [
        'method' => 'PATCH',
        'url' => ['films', $film->id],
        'files' => true,
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('release_date') ? 'has-error' : ''}}">
                {!! Form::label('release_date', 'Release Date: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('release_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('release_date', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('rating') ? 'has-error' : ''}}">
                {!! Form::label('rating', 'Rating: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('rating', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('rating', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('ticket_price') ? 'has-error' : ''}}">
                {!! Form::label('ticket_price', 'Ticket Price: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('ticket_price', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('ticket_price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
                {!! Form::label('country', 'Country: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('country', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('genre') ? 'has-error' : ''}}">
                {!! Form::label('genre', 'Genre: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('genre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('genre', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">               
                <div class="form-group">
                    <label class="col-sm-3 control-label">Photo:</label >			                    
                    <div class="col-sm-4">
                        <input type="file" name="photo" class="form-control" value="{{$film->photo}}" {!!$film->photo ? '' : 'required="true"' !!} >      
                        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                    </div>    
                
                    <div class="col-sm-3">
                        <img src="{{$film->photo}}" class="film-img">
                    </div>
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection