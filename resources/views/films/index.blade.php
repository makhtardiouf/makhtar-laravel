@extends('layouts.app') 
@section('title') Films List @stop 
 
 {{-- Frontend page /films/ to show all films through API. 1 film per 1 page. --}} 

@section('content')

<h3>Films List</h3>

@if(empty($films))
    <h5>No films data recorded yet</h5>
@endif

@foreach($films as $film) 
    @include('partials.film')   
@endforeach

@endsection 