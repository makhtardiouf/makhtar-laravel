@extends('layouts.app') 
@section('content')

<div class="flex-center">
	<div class="content jumbotron">
		<h3>Makhtar, Film CRUD app with Laravel 5.4</h3>

		<div class="row">
			<a class="btn btn-primary pull-right btn-sm" href="{{ url('films') }}">Films list</a>

			<a class="btn btn-primary pull-right btn-sm" href="{{ url('films/create') }}">Add New Film</a>
		</div>

		<div class="links" style="padding-top:50px;">
			<a href="https://laravel.com/docs" target="_blank">Documentation</a>

			<a href="https://bitbucket.org/makhtardiouf/makhtar-laravel" target="_blank">Repository</a>
		</div>

	</div>
</div>


@endsection