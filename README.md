# Demo film registration app

#Description#
Web application for registering film-related data, using the **Laravel 5.4 framework, and a MySQL 5.7 database** (here named 'makhtar-laravel'). It supports standard CRUD operations: Create, Update, Delete and List films data.

* All links to the different pages are controlled by the makhtar-laravel/routes/web.php file,
with restrictions set to certain pages.

* As requested, a redirect is set from / to /films/, to navigate to the listing page, showing **1 film per 1 page**. 

* The FilmController@show method is implemented to handle both access via Integer Film IDs (e.g. /films/1) and Film slugs (e.g. /films/conan)

* The authentication part used Laravel's default implementation that comes with the framework.
* Registered users can post comment on film pages.

* A DB seeder is included to auto-generate fake data for testing.
* The hidden file makhtar-laravel/.env defines the parameters used by this application.

# Installation

git clone https://bitbucket.org/makhtardiouf/makhtar-laravel

cd makhtar-laravel

composer install

php artisan key:generate

* Add MySQL permissions for user "demo" by running the following SQL statement:

GRANT ALL PRIVILEGES on `makhtar-laravel`.* TO demo@'localhost' IDENTIFIED BY ';demo;' ;

* Import the database dump database/makhtar-laravel20171125.sql file, or run this command 
* to start from scratch:

php artisan migrate --seed 

Or 

php artisan db:seed   # This will add 3 films and 2 users to the DB


# Testing
You can use the php development server to access the UI quickly, e.g.:

cd makhtar-laravel

php artisan serve

Then open a browser tab with the address http://localhost:8000

*** Login with user "elmakdi@gmail.com" and password ";demo;"

