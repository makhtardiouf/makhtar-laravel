<?php

namespace App;

use Eloquent as Model;
use Carbon\Carbon;

/**
 * Class Comment for Films
 * @package App
 */
class Comment extends Model
{
    public $table = 'comments';
    public $fillable = [
        'user_id',
        'target_id',
        'id',
        'content',
        'type'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required, numeric',
        'target_id' => 'required, numeric',      
        'content' => 'min:5'
    ];   

     public function user(){

        return $this->belongsTo('App\User');
    }
 
 public function getDateDiff() {
     return Carbon::createFromTimeStamp(strtotime($this->updated_at))->diffForHumans();
 }
}
