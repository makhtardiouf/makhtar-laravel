<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Film extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'films';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'release_date', 'rating', 'ticket_price', 'country', 'genre', 'photo'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
