<?php

namespace App\Http\Controllers;

use App\Film;
use App\Comment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;
use Session;

class FilmController extends Controller
{
    public $nItems = 1; // 1 film per page
    public $imgPath = 'uploads'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
        
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $films = Film::orderBy('id', 'desc')
                    ->paginate($this->nItems);

        $comments = Comment::all();
        return view('films.index', compact('films', 'comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('films.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required', 'release_date' => 'required', 'rating' => 'required', 'ticket_price' => 'required', 'country' => 'required', 'genre' => 'required', 'photo' => 'required', ]);

        if ($request->hasFile('photo')) {
            $request->photo = "/".$this->uploadImage($request->photo, $this->imgPath);
        } elseif (!$request->photo) {
            $request->photo = "http://via.placeholder.com/350x150";
            Log::warning("No project image file uploaded. Using default");
        }

        $film = Film::create($request->all());
        $film->photo = $request->photo;
        $film->save();

        Session::flash('message', 'Film added!');
        Session::flash('status', 'success');

        return redirect('films');
    }

    /**
     * Display the specified resource.
     *
     * @param  $id can be int or slug (string)
     *
     * @return Response
     */
    public function show($id)
    {
        if(!is_int($id)) {
            // Get film with slug = id string
            $film = Film::find(1)
                    ->where('name', 'like', "$id")
                    ->first();

            // For comments retrieval
            Log::debug("Film $film");
            $id = 0;
            if(!empty($film))
                $id = $film->id;
        } else {
            $film = Film::findOrFail($id);
        }
        if(empty($film)) {
            return abort(404);
        }

        $comments = Comment::where('target_id', $id)
        ->where('type', 'films')
        ->orderBy('id', 'desc')
        ->take(10)
        ->get();

        return view('films.show', compact('film', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $film = Film::findOrFail($id);

        return view('films.edit', compact('film'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required', 'release_date' => 'required', 'rating' => 'required', 'ticket_price' => 'required', 'country' => 'required', 'genre' => 'required', 'photo' => 'required', ]);

        if ($request->hasFile('photo')) {
            $request->photo = "/".$this->uploadImage($request->photo, $this->imgPath);
        } elseif (!$request->photo) {
            $request->photo = "http://via.placeholder.com/350x150";
            Log::warning("No project image file uploaded. Using default");
        }

        $film = Film::findOrFail($id);
        $film->update($request->all());
        $film->photo = $request->photo;
        $film->save();
      
        Session::flash('message', 'Film updated!');
        Session::flash('status', 'success');

        return redirect('films');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $film = Film::findOrFail($id);

        $film->delete();

        Session::flash('message', 'Film deleted!');
        Session::flash('status', 'success');

        return redirect('films');
    }


    /**
     * Handle image uploading
     */
    public function uploadImage($file, $path)
    {
        $image_name =  time().'_'.$file->getClientOriginalName();
        $store_path = public_path($path);
    
        File::exists($store_path) or File::makeDirectory($store_path);
    
        $img = Image::make($file);
        $img->save($store_path.$image_name);

        Log::debug("\n\n**** Save image $image_name");
        return $this->imgPath.$image_name;
    }
}
