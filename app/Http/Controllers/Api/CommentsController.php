<?php

namespace App\Http\Controllers\Api;

use App\Film;
use App\Comment;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;

/**
 * Polymorphic class to handle Comments on different objects, e.g. Films, ...
 *
 */
class CommentsController extends Controller
{
    /**
    * Retrieve last few comments on this object
    */
    public function index($oid)
    {
        $type = Input::get('type');
        $maxComments = 20;
        $comments = '';
        
        try {
            $comments = Comment::where('target_id', $oid)
                ->where('type', $type)
                ->orderBy('id', 'desc')
                ->take($maxComments)
                ->get();
                
        } catch (Exception $e) {
            Log::error("Error retrieving comments:\n".$e->getMessage());
        }

        Log::debug("Retrieved ".count($comments). " comments");
        return response()->json($comments);
    }
    
    /**
    * Submit a comment on this Film
    * @todo Access control with Passport
    */
      public function comment(Request $request)
      {
          try {
              $this->validate($request, $this->rules);
               
              if (empty($request->user_id) && !Auth::guard('user')->check()) {
                  $msg = "Error: comment denied for anonymous user";
                  Log::error($msg.json_encode($request->all()));
                  return response()->json($msg);
              }

              $target = '';
              $com = '';

              switch ($request->type) {
                    // Films
                    default:
                        $target = Film::findOrFail($request->target_id);
                        $com = new Comment();
                        $com->type = "Films";
                        $com->target_id = $request->target_id;
                }
                            
              $com->user_id = $request->user_id;
              $com->content = $request->content;
              $com->save();

              $msg = "Comment saved for ".$request->type."/".$request->target_id;
              Log::debug($msg);
              return response()->json($msg);
          } catch (Exception $e) {
              Log::error("Error saving comment for uid". $request->user_id . " \n".$e->getMessage());
              return response()->json("Error");
          }
      }

      /**
     * Validation rules
     */
    protected $rules =  [                       
        'target_id' => 'required|numeric',            
        'content' => 'required|max:1000'
    ];

}
