<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Redirect from / to /films/
Route::get('/', function () {
    return redirect()->guest(route('films.index'));
});

Route::get('/home', function () {
    return view('welcome');
});

Route::group(['prefix' => 'auth'], function() {
    Auth::routes();

    Route::get('/logout', function(){
        try {        
            $user = Auth::guard('web');

            if ($user) {                  
                Auth::logout();
                session()->flush();
            } else {
                session()->flush();
            }
            return view('welcome');
        } catch(Exception $e) {
            
            return view('welcome');
        }
    });

});

Route::group(['middleware' => ['auth:web']], function () {
	Route::resource('films', 'FilmController', ['except' => ['show', 'index']]);
});

Route::group(['prefix' => 'films'], function() {
    Route::get('/', 'FilmController@index')->name('films.index');

    // $id can be int or slug (string)
    Route::get('/{id}', 'FilmController@show')->name('films.show');
});

