<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// List comments made on films
Route::get('/films/{id}/comments', 'Api\CommentsController@index');

// Submit a comment to a film
Route::post('/films/{id}/comment', 'Api\CommentsController@comment');
